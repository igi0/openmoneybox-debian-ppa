# OpenMoneyBox Debian PPA

Debian PPA for OpenMoneyBox releases

**Install on Debian 12 Bookworm:**

    curl -s --compressed "https://gitlab.com/igi0/openmoneybox-debian-ppa/-/raw/master/KEY.gpg" | sudo apt-key add -
    sudo curl -s --compressed -o /etc/apt/sources.list.d/openmoneybox.list "https://gitlab.com/igi0/openmoneybox-debian-ppa/-/raw/master/bookworm/filelist.list"
    sudo apt update
    sudo apt install openmoneybox


**Install on Raspbian 12 Bookworm:**

    curl -s --compressed "https://gitlab.com/igi0/openmoneybox-debian-ppa/-/raw/master/KEY.gpg" | sudo apt-key add -
    sudo curl -s --compressed -o /etc/apt/sources.list.d/openmoneybox.list "https://gitlab.com/igi0/openmoneybox-debian-ppa/-/raw/master/raspbian-bookworm/filelist.list"
    sudo apt update
    sudo apt install openmoneybox


**Install on Debian 11 Bullseye:**

    curl -s --compressed "https://gitlab.com/igi0/openmoneybox-debian-ppa/-/raw/master/KEY.gpg" | sudo apt-key add -
    sudo curl -s --compressed -o /etc/apt/sources.list.d/openmoneybox.list "https://gitlab.com/igi0/openmoneybox-debian-ppa/-/raw/master/bullseye/filelist.list"
    sudo apt update
    sudo apt install openmoneybox


**Install on Raspbian 11 Bullseye:**

    curl -s --compressed "https://gitlab.com/igi0/openmoneybox-debian-ppa/-/raw/master/KEY.gpg" | sudo apt-key add -
    sudo curl -s --compressed -o /etc/apt/sources.list.d/openmoneybox.list "https://gitlab.com/igi0/openmoneybox-debian-ppa/-/raw/master/raspbian-bullseye/filelist.list"
    sudo apt update
    sudo apt install openmoneybox
